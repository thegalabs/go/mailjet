//go:build !no_mailjet
// +build !no_mailjet

package mailjet

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	"github.com/rs/zerolog/log"
)

func (c *defaultClient) Send(e *Email) error {
	var b body
	b.Messages = append(b.Messages, *e)

	body, err := json.Marshal(b)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", c.url("v3.1/send"), bytes.NewBuffer(body))
	if err != nil {
		return err
	}

	req.SetBasicAuth(c.key, c.secret)

	resp, err := c.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		respBody, _ := ioutil.ReadAll(resp.Body)
		log.Error().Str("body", string(respBody)).Msgf("Could not send email with status: %d", resp.StatusCode)
		return errors.New("could not send email")
	}

	return nil
}
