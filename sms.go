//go:build !no_mailjet
// +build !no_mailjet

package mailjet

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	"github.com/rs/zerolog/log"
)

func (c *defaultClient) SendSMS(text string, to string, from string) error {
	if c.token == "" {
		return errors.New("tried sending sms with an empty token")
	}

	body := map[string]string{
		"Text": text,
		"To":   to,
		"From": from,
	}

	raw, err := json.Marshal(body)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", c.url("v4/sms-send"), bytes.NewBuffer(raw))
	if err != nil {
		return err
	}
	req.Header.Add("Authorization", "Bearer "+c.token)
	req.Header.Add("Content-Type", "application/json")

	resp, err := c.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		respBody, _ := ioutil.ReadAll(resp.Body)
		log.Error().Str("body", string(respBody)).Msgf("Could not send sms with status: %d", resp.StatusCode)
		return errors.New("could not send email")
	}

	return nil
}
